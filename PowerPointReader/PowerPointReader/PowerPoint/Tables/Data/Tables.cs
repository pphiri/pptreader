﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PowerPointReader.PowerPoint
{
    public class PptTable
    {
        public IEnumerable<PptTableRow> Rows { get; set; }
    }

    public class PptTableRow
    {
        public IEnumerable<PptTableCell> Cells { get; set; }
        
    }
    public class PptTableCell
    {
        public string CellValue { get; set; }
    }
}