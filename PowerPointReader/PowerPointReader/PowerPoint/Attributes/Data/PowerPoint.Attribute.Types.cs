﻿using System.Collections.Generic;

namespace PowerPointReader.PowerPoint
{
    public class PowerPointAttributeTypes
    {
        public string Property { get; set; }
        public Dictionary<string, object> Attributes { get; set; }
    }
}
