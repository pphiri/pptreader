﻿using System;
using System.Linq;
using System.Collections.Generic;
using DocumentFormat.OpenXml.Presentation;
using A = DocumentFormat.OpenXml.Drawing;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml;
using System.Text;
using PowerPointReader.Utilities;

namespace PowerPointReader.PowerPoint
{
    class PowerPointTables : PptTable
    {
        private Slide _pptSlide; 
        public PowerPointTables(SlidePart slide)
        {
            _pptSlide = slide.Slide;
        }

        public IEnumerable<PptTable> GetTables()
        {
            List<PptTable> slideTables = new List<PptTable>(); 
            IEnumerable<A.Table> tables = _pptSlide.Descendants<A.Table>();

            foreach (var table in tables)
            {
                List<PptTableRow> tableRows = new List<PptTableRow>();
                IEnumerable<A.TableRow> rows = table.Descendants<A.TableRow>();
                foreach (var row in rows)
                {
                    List<PptTableCell> tableCells = new List<PptTableCell>();
                    IEnumerable<A.TableCell> cells = row.Descendants<A.TableCell>();
                    cells.ToList().ForEach(x => tableCells.Add(new PptTableCell { CellValue = x.InnerText.ToString() }));
                    tableRows.Add(new PptTableRow { Cells = tableCells as IEnumerable<PptTableCell>});
                }
                slideTables.Add(new PptTable {  Rows = tableRows});
            }

            return slideTables as IEnumerable<PptTable>;
        }

        private IEnumerable<A.TableRow> GetRows(A.Table table)
        {
            IEnumerable<A.TableRow> rows = table.Descendants<A.TableRow>();

            if (rows.Count() == 0) throw new ArgumentException("Table contained no rows");

            return rows;
        }

        private IEnumerable<A.TableCell> GetCells(A.TableRow row)
        {
            IEnumerable<A.TableCell> rows = row.Descendants<A.TableCell>();

            if (rows.Count() == 0) throw new ArgumentException("Table row contained no cells");

            return rows;
        }
    }
}
