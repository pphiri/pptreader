﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PowerPointReader.Utilities;
using PowerPointReader.PowerPoint;

namespace PowerPointReader
{
    class Program
    {
        static void Main(string[] args)
        {
            string path = @"C:\Temp\PowerPointDocs\HCSC_Automation_Example.pptx";
            int slideNum = 1;

            PptReader pptReader = new PptReader(path);

            var slide = pptReader.GetSlide(slideNum);

            var slideData = pptReader.GetSlideData(slideNum);
        }
    }
}
