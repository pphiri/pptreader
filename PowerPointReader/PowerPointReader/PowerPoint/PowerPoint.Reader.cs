﻿using System;
using System.Linq;
using System.Collections.Generic;
using DocumentFormat.OpenXml.Presentation;
using A = DocumentFormat.OpenXml.Drawing;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml;
using System.Text;
using PowerPointReader.Utilities;

namespace PowerPointReader.PowerPoint
{
    class PptReader : FileAccess
    {
        private PresentationDocument _document;

        public PptReader(string docPath)
        {
            _document = OpenFile(docPath);
        }

        public IEnumerable<SlidePart> GetSlides()
        {
            var presentationParts = _document.PresentationPart;
            var slides = presentationParts.SlideParts;

            if (slides.Count() == 0) throw new NullReferenceException("Found no slides in document");
                                  
            return slides;
        }

        public SlidePart GetSlide(int slideNumber)
        {
            var presentationPart = _document.PresentationPart;
            var slideIds = presentationPart.Presentation.SlideIdList.ChildElements;
            var relationshipId = (slideIds[slideNumber] as SlideId).RelationshipId;

            SlidePart slide = (SlidePart)presentationPart.GetPartById(relationshipId);


            return slide;
        }

        public SlidePart GetSlideData(int slideNumber)
        {
            StringBuilder paragraphText = new StringBuilder();
            ShapeProperties shapeProps = new ShapeProperties();

            var presentationPart = _document.PresentationPart;
            var slideIds = presentationPart.Presentation.SlideIdList.ChildElements;
            var relationshipId = (slideIds[slideNumber] as SlideId).RelationshipId;

            SlidePart slide = (SlidePart)presentationPart.GetPartById(relationshipId);

            if (slide is null) throw new NullReferenceException("slide is empty or doesn't exist");

            var allTables = new PowerPointTables(slide).GetTables();
            
            return slide;            
        }
    }
}
