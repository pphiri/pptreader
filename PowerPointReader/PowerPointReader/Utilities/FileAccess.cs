﻿using DocumentFormat.OpenXml.Packaging;
using System;
using System.IO;
using System.IO.Packaging;

namespace PowerPointReader.Utilities
{
    class FileAccess
    {   
        public PresentationDocument OpenFile(string path)
        {
            Stream file = File.Open(path, FileMode.Open);

            PresentationDocument pptDoc = PresentationDocument.Open(file, false);
            return pptDoc;
        }
    }
}
