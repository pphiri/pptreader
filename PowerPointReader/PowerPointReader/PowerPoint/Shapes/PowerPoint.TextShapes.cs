﻿using System;
using System.Linq;
using System.Collections.Generic;
using DocumentFormat.OpenXml.Presentation;
using A = DocumentFormat.OpenXml.Drawing;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml;
using System.Text;
using PowerPointReader.Utilities;

namespace PowerPointReader.PowerPoint
{
    class PowerPointText
    {
        public IEnumerable<A.Text> GetTextShapes(SlidePart slide)
        {
            IEnumerable<A.Text> textShapes = slide.Slide.Descendants<A.Text>();
            
            return textShapes;
        }

        public String GetAllTextFromSlide(SlidePart slide)
        {
            StringBuilder slideText = new StringBuilder();

            IEnumerable<A.Text> textShapes = slide.Slide.Descendants<A.Text>();
            
            foreach (A.Text subText in textShapes)
            {
                slideText.Append(subText.Text);
                slideText.Append(" ");
            }

            return slideText.ToString();
        }
    }
}
