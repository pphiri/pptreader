﻿using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Presentation;
using System;
using System.Collections.Generic;
using System.Linq;

namespace PowerPointReader.PowerPoint
{
    class PowerPointAttributes : PowerPointAttributeTypes
    {

        public List<PowerPointAttributeTypes> GetShapeAttributes(OpenXmlElement shape)
        {
            List<PowerPointAttributeTypes> attrList = new List<PowerPointAttributeTypes>();
            OpenXmlElementList shapeProps = shape.ChildElements.Single(x => x.GetType().Equals(typeof(ShapeProperties))).ChildElements;

            if (shapeProps.Count > 0)
            {
                foreach (var prop in shapeProps)
                {
                    var childProps = GetChildProps(prop);
                    attrList.Add(new PowerPointAttributeTypes { Property = prop.LocalName, Attributes = childProps });
                }
            }
            return attrList;
        }

        private Dictionary<String, object> GetChildProps(object element)
        {
            Dictionary<String, object> elemenetPropsDict = new Dictionary<string, object>();
            var elementProps = element.GetType().GetProperties();

            foreach (var prop in elementProps)
            {
                try
                {
                    var value = element.GetType().GetProperty(prop.Name).GetValue(element, null);
                    elemenetPropsDict.Add(prop.Name, value);
                }
                catch (ArgumentException ex)
                {
                    elemenetPropsDict.Add(prop.Name, null);
                }
            }

            return elemenetPropsDict;
        }
    }
}